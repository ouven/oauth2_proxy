module github.com/pusher/oauth2_proxy

go 1.12

require (
	cloud.google.com/go v0.16.0 // indirect
	github.com/BurntSushi/toml v0.3.1
	github.com/alecthomas/gometalinter v3.0.0+incompatible // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/coreos/go-oidc v0.0.0-20171026214628-77e7f2010a46
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/golang/protobuf v0.0.0-20171113180720-1e59b77b52bf // indirect
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mbland/hmacauth v0.0.0-20170912224942-107c17adcc5e
	github.com/mreiferson/go-options v0.0.0-20190302064952-20ba7d382d05
	github.com/nicksnyder/go-i18n v1.10.0 // indirect
	github.com/pelletier/go-toml v1.3.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/pquerna/cachecontrol v0.0.0-20171018203845-0dec1b30a021 // indirect
	github.com/stretchr/testify v1.1.4
	github.com/yhat/wsutil v0.0.0-20170731153501-1d66fa95c997
	golang.org/x/crypto v0.0.0-20171113213409-9f005a07e0d3
	golang.org/x/net v0.0.0-20171115151908-9dfe39835686
	golang.org/x/oauth2 v0.0.0-20171106152852-9ff8ebcc8e24
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894 // indirect
	google.golang.org/api v0.0.0-20171116170945-8791354e7ab1
	google.golang.org/appengine v1.0.0 // indirect
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c // indirect
	gopkg.in/fsnotify/fsnotify.v1 v1.2.11
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/square/go-jose.v2 v2.1.3
)
