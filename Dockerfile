FROM golang as build
WORKDIR /build
COPY . .
ENV CGO_ENABLED=0
ENV GOARCH=386
ENV GOOS=linux
RUN go test ./...
RUN go build
RUN touch jwt_signing_key.pem

FROM alpine:latest as tls
RUN apk --no-cache add tzdata zip ca-certificates
WORKDIR /usr/share/zoneinfo
# -0 means no compression.  Needed because go's
# tz loader doesn't handle compressed data.
RUN zip -r -0 /zoneinfo.zip .

FROM scratch
# the timezone data:
ENV ZONEINFO /zoneinfo.zip
COPY --from=tls /zoneinfo.zip /
# the tls certificates:
COPY --from=tls /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /build/jwt_signing_key.pem /etc/ssl/private/jwt_signing_key.pem
COPY --from=build /build/oauth2_proxy /

EXPOSE 80
EXPOSE 443

ENV OAUTH2_PROXY_HTTP_ADDRESS=http://0.0.0.0:80
ENV OAUTH2_PROXY_HTTPS_ADDRESS=https://0.0.0.0:443

ENTRYPOINT ["/oauth2_proxy"]
